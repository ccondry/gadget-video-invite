var finesse = window.finesse;
var clientLogs;
if (!finesse) {
	throw new Error("Finesse library is not available");
} else {
	var finesse = finesse || {};
	finesse.gadget = finesse.gadget || {};
	finesse.container = finesse.container || {};
	clientLogs = finesse.cslogger.ClientLogger || {};
}
var cache = {
	requestTypes: {
		'video-invite': {
		}
	}
}
/** @namespace */
finesse.modules = finesse.modules || {};
finesse.modules.VideoInvite = (function ($) {
	var _cfg, _prefs;
	//global for Finesse Utilities (utility function object)
	var _util;
	var numDialogs = 0;	     // used to count the calls (dialogs)
	var callvars = new Array();  // the callvars array of callvariables
	//gmaps center location
	var resultsLoaded = false;
	// requestTypes api response cache object

	var user, states, dialogs;

	//send SMS
	function sendSms() {
		sendTropoSms($("#SmsAni").val(),$('#SmsContent').val());
	}

	function sendTropoSms(ani,message){
		var tropoToken = config.tropo.token;
		var tropoApiUrl = "https://api.tropo.com/1.0/sessions";
		var smsUrl = tropoApiUrl + "?action=create&token="
		+ tropoToken + "&ani=" + ani + "&content="	+ encodeURIComponent(message);
		clientLogs.log("sendTropoSms URI: " + smsUrl);
		try {
			doProxiedRequest(smsUrl, "GET", true, function (rsp) {
				// clientLogs.log("got call types: " + rsp.data);
				console.log("VideoInvite-coty sendTropoSms rsp raw data = ", rsp)
				$('#sms_response').html("<p>SMS Sent.</p>")
			}, null, function (rsp) {
				console.log("VideoInvite-coty sendTropoSms error", rsp)
				clientLogs.log("sendSms error" + rsp.data);
				$('#sms_response').html("<p>Error sending SMS.</p>");
			});
		} catch (ex) {
			clientLogs.log("Caught exception: " + ex);
		}
	}

	function addRemMessageTemplate() {
		var remUrl = makeRemAppUrl({
			host: config.rem.host,
			protocol: config.rem.protocol,
			port: config.rem.port,
			destination: config.rem.destination,
			encodeUui: config.rem.encodeUui, //default true
			correlationId: 'assist-'+$("#SmsAni").val(),
			uui: getAgentId() // route to this agent ID
		});

		shortenUrl(remUrl, function(shortUrl) {
			var message = "Click here to start video call: " + shortUrl;
			clientLogs.log("REM SMS template = " + message);
			cache.requestTypes["video-invite"].messageTemplate = message;
		})

		function makeRemAppUrl(params){
			var base = config.rem.baseAppUrl;
			var s = '?host='+params.host+'&protocol='+params.protocol+'&port='+params.port+'&destination='+params.destination;
			if(typeof params.encodeUui !== 'undefined')
			s += '&encodeUui='+params.encodeUui;
			if(typeof params.uui !== 'undefined')
			s += '&uui='+params.uui;
			if(typeof params.correlationId !== 'undefined') {
				s += '&correlationId='+params.correlationId;
			}
			return base + s;
		}

		function shortenUrl(longUrl,callback) {
			var params = [
				{
					name:'token',
					value: config.rem.linkToken
				},
				{
					name:'longUrl',
					value: encodeURIComponent(longUrl)
				}
			];
			clientLogs.log("making getCallPage URL");
			var uri = makeUri(config.rem.linkUrl,params);
			clientLogs.log("shortUrl URI: " + uri);
			$.ajax(uri)
			.done(function(rsp){
				console.log("coty shortenurl rsp",rsp);
				clientLogs.log("shortened URL = " + rsp.url);
				callback(rsp.url);
			})
			.fail(function (xhr, something, error) {
				clientLogs.log("sendSms error" + xhr.content);
				$('#sms_response').html("<p>Error sending SMS.</p>");
			});
		}
	}

	function updateSmsOptions(requestTypes, selectedType) {
		console.log("VideoInvite-coty updateSmsOptions - requestTypes = ", requestTypes);
		try {
			// clear current SMS options
			$("#SMSTemplate").empty();
			// add a default "select" option
			var defaultOption = $('<option></option>').val("null").html("Select SMS Template");
			$("#SMSTemplate").append(defaultOption);
			// add all the options from the result set
			$.each(requestTypes, function(i, requestType) {
				var option = $('<option></option>').val(requestType.requestType).html(requestType.name);
				$("#SMSTemplate").append(option);
				// is this the selected option?
				if(requestType.requestType === selectedType) {
					// set this as the selected option
					option.attr("selected", "selected");
				}
			});
			var VideoInviteOption = $('<option></option>').val("video-invite").html("Video Invite");
			$("#SMSTemplate").append(VideoInviteOption);
		} catch (e) {
			clientLogs.log(e);
		}
	}

	function updateSmsTemplate(requestTypes, requestType) {
		try {
			// is the current call's request type match one in our list?
			if(typeof requestTypes[requestType] != "undefined") {
				// set the sms template option using the incoming call's request type
				$('#SMSTemplate').val(requestType);
				// update the sms template message
				finesse.modules.VideoInvite.changeMessage();
			}
		} catch(e) {
			clientLogs.log(e);
		}
	}

	function makeUri(uri, params){
		//var uri = server + page;
		var params = params || [];
		for (var i=0; i < params.length; i++) {
			if (i !== 0) {
				uri += '&';
			} else {
				uri += '?';
			}
			uri += params[i].name + '=' + params[i].value;
		}
		return uri;
	}

	function getAgentId() {
		return finesse.gadget.Config.id;
	}

	function render() {
		var currentState = user.getState();

		if (numDialogs==1) {
			getCall(callvars["callVariable1"]);
		}
		else {
			// we don't have a call yet so do nothing
			clientLogs.log("numDialogs !=1. it's: " + numDialogs);
		}
	}

	/** @scope finesse.modules.VideoInvite */
	return {
		/**
		* Performs all initialization for this gadget
		*/
		init : function () {
			//For SSO deployment, Gadgets should use the finesse provided config
			//object instead of creating their own instance
			_cfg = finesse.gadget.Config;

			// Initiate the ClientServices and load the config object. ClientServices
			// are initialized with a reference to the current configuration.
			// For SSO deployments , gadgets need to initialize clientservices for
			// loading config object properly.Without initialization, SSO mode will not work
			finesse.clientservices.ClientServices.init(_cfg);
			_prefs = new gadgets.Prefs();
			clientLogs.init(gadgets.Hub, "VideoInvite-coty", finesse.gadget.Config); //this gadget id will be logged as a part of the message

			containerServices = finesse.containerservices.ContainerServices.init();
			// containerServices.addHandler(finesse.containerservices.ContainerServices.Topics.ACTIVE_TAB, _handleTabVisible);
			// containerServices.addHandler(finesse.containerservices.ContainerServices.Topics.GADGET_VIEW_CHANGED_EVENT, _handleGadgetViewChanged);
			// containerServices.addHandler(finesse.containerservices.ContainerServices.Topics.MAX_AVAILABLE_HEIGHT_CHANGED_EVENT, _handleMaxAvailableHeightChange);

			finesse.containerservices.ContainerServices.makeActiveTabReq();
			dialogs = [];
			dialog = null;

			user = new finesse.restservices.User({
				id: finesse.gadget.Config.id
				// onLoad : handleUserLoad,
				// onChange : handleUserChange
			});

			_util = finesse.utilities.Utilities;


			states = finesse.restservices.User.States;
			/**
			* init user code
			*/
			$(document).ready(function(){
				$('#SmsAni').on('change', function () {
					var val = $('#SmsAni').val()
					addRemMessageTemplate();
					clientLogs.log("Adding REM message template to the SMS templates list");
					updateSmsOptions(cache.requestTypes, 'video-invite');
				})
			})
			clientLogs.log("Completed init");
		},

		changeMessage: function(e) {
			if(typeof cache != "undefined") {
				if(typeof cache.requestTypes != "undefined") {
					var option = $('#SMSTemplate :selected').val();
					clientLogs.log("SMS Message option: " + option);
					//var optionName = $('#SMSTemplate :selected').attr("name");
					$('#SmsContent').val( cache.requestTypes[option].messageTemplate );
				} else {
					clientLogs.log("changeMessage: cache.requestType was undefined.");
				}
			} else {
				clientLogs.log("changeMessage: cache was undefined.");
			}
		},
		sendSms: function() {
			//TODO write function
			sendSms()
		}
	};
}(jQuery));


function doProxiedRequest(url, method, authRequired, success, postdata, errorCallback)
{
	var fullOrigin=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
	var urlNoCache = url;
	if (url.indexOf("?") > -1) {
		urlNoCache = urlNoCache + "&";
	} else {
		urlNoCache = urlNoCache + "?";
	}
	urlNoCache = urlNoCache + "nocache=" + generateUUID() + "&origin=" + fullOrigin;

	var params = {};
	if (authRequired == true)
	{
		params[gadgets.io.RequestParameters.HEADERS] = {
			//"Authorization": "Basic " + btoa(finesse.gadget.Config.id + ":" + finesse.gadget.Config.password)
			"Authorization": "Basic " + finesse.gadget.Config.authorization,
			"Content-Type": "application/json"

		}
	}

	params[gadgets.io.RequestParameters.METHOD] = method;

	if (typeof postdata !== 'undefined' && postdata !== null)
	{
		params[gadgets.io.RequestParameters.POST_DATA]= postdata;
	}

	gadgets.io.makeRequest(urlNoCache, function(resp) {
		if(resp.rc < 200 || resp.rc >= 300)
		{
			if (errorCallback)
			{
				errorCallback(resp);
			}
			else
			{
				var message = "Unexpected response '" + resp.rc + "' to "  + method + " request to: " + url;
				_logger.log(message);
				displayInfoMessage(message);
			}
		}
		else
		{
			success(resp)
		}

	}, params);
}

function generateUUID() {
	function _p8(s) {
		var p = (Math.random().toString(16)+"000000000").substr(2,8);
		return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
	}
	return _p8() + _p8(true) + _p8(true) + _p8();
}
