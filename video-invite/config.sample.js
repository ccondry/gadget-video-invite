var config = {
  tropo: {
    token: "abcd1234"
  },
  rem: {
    linkToken: "abcd1234",
    linkUrl: "https://custom-shortlink-provider.com/create-link",
    baseAppUrl: "rem://call/call",
    host: "rem1.dcloud.cisco.com",
    protocol:"https",
    port:"8443",
    destination:"sip:7700@dcloud.cisco.com",
    encodeUui: "true" //default true
  }
}
