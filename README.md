# gadget-video-invite
Cisco Finesse gadget to send an SMS with a short link that activates an REM call to the agent

## Installation
copy video-invite/config.sample.js to video-invite/config.js and fill in with your configuration values
